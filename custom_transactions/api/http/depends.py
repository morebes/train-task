from uuid import UUID
from fastapi import Depends
from fastapi.requests import Request

from core.db.db import DB
from core.db.transaction import Transaction
from domains.item.use_case import ItemUseCase, TransactionItemUseCase


class DIContainer:
    @staticmethod
    def get_db(request: Request) -> DB:
        return request.app.state.db

    @staticmethod
    def get_transaction_id(request: Request) -> UUID:
        return request.cookies.get("X-Transaction-id")

    @staticmethod
    def get_transaction(
        db: DB = Depends(get_db),
        transaction_id: UUID = Depends(get_transaction_id),
    ) -> Transaction | None:
        if transaction := getattr(db, "_DB__transactions").get(transaction_id):
            return transaction
        return None

    @staticmethod
    def get_item_usecase(
        db: DB = Depends(get_db),
        transaction_id: UUID = Depends(get_transaction_id),
        transaction: UUID = Depends(get_transaction),
    ) -> ItemUseCase | TransactionItemUseCase:
        if transaction_id and transaction:
            return TransactionItemUseCase(transaction)
        return ItemUseCase(db=db)
