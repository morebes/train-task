from uuid import UUID
from fastapi import APIRouter, Depends, Response

from api.http.depends import DIContainer, DB

router = APIRouter(prefix="/transaction")


@router.get("/begin/")
def begin_transaction(db: DB = Depends(DIContainer.get_db)):
    transaction_id = db.begin()

    response = Response()
    response.set_cookie("X-Transaction-id", transaction_id)
    return response


@router.get("/commit/")
def commit_transactions(db: DB = Depends(DIContainer.get_db)) -> int:
    return db.commit()


@router.get("/rollback/")
def delete_data(
    response: Response,
    transaction_id: UUID = Depends(DIContainer.get_transaction_id),
    db: DB = Depends(DIContainer.get_db),
) -> None:
    db.rollback(transaction_id)
    return response.delete_cookie("X-Transaction-id")
