from fastapi import APIRouter, Depends

from api.http.depends import DIContainer, ItemUseCase, TransactionItemUseCase

router = APIRouter(prefix="/data")


@router.get("/")
def fetch_data(
    usecase: ItemUseCase | TransactionItemUseCase = Depends(
        DIContainer.get_item_usecase
    ),
):
    return usecase.fetch_items()


@router.post("/create")
def create_data(
    value: str,
    usecase: ItemUseCase | TransactionItemUseCase = Depends(
        DIContainer.get_item_usecase
    ),
) -> int:
    return usecase.create_item({"value": value})


@router.delete("/delete")
def delete_data(
    id: int,
    usecase: ItemUseCase | TransactionItemUseCase = Depends(
        DIContainer.get_item_usecase
    ),
) -> None:
    return usecase.delete_item(id)
