from fastapi import FastAPI

from core.db.db import DB
from api.http import v1

app = FastAPI()
setattr(app.state, "db", DB())

app.include_router(v1)
