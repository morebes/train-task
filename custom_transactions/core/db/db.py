from uuid import UUID

from .transaction import Transaction
from .incrementor import Incrementor


class DB:
    def __init__(self) -> None:
        self.__data = {}
        self.__transactions = {}

    def get(self, id: int | None = None) -> dict | None:
        if id:
            return self.__data.get(id)
        return self.__data

    def create(self, create_obj) -> int:
        id = Incrementor.get_next_id()
        self.__data[id] = {"id": id, **create_obj}

        print(self.__data)
        return id

    def delete(self, id) -> None:
        if not self.get(id):
            raise ValueError("Id is not definite")
        del self.__data[id]
        print(self.__data)

    def begin(
        self,
    ) -> UUID:
        new_transaction = Transaction(db=self)

        self.__transactions[new_transaction.id] = new_transaction

        print(self.__transactions)
        return new_transaction.id

    def commit(self, transaction_id):
        transaction: Transaction = self.__transactions[transaction_id]

        self.__data = transaction._merge_states()

    def rollback(self, transaction_id) -> None:
        if self.__transactions.get(transaction_id):
            del self.__transactions[transaction_id]
